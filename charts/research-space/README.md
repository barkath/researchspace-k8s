# ResearchSpace Helm Chart

Deploy your own private ResearchSpace instance with a Blazegraph triple store and Digilib IIIF image server.

Please see [https://github.com/researchspace/researchspace](https://github.com/researchspace/researchspace) for further information on ResearchSpace.

## Prerequisites

* Kubernetes with extensions/v1beta1 available
* A persistent storage resource and RW access to it
* Kubernetes StorageClass for dynamic provisioning
* Nginx ingress controller with a load balancer set up

## Configuration

For a more robust solution supply helm install with a custom values.yaml

The following table lists common configurable parameters of the chart and their default values. See [values.yaml](values.yaml) for all available options.

| Parameter                        | Description                                                   | Default                     |
|----------------------------------|---------------------------------------------------------------|-----------------------------|
| `fqdn`                           | Domain name for your ResearchSpace service                    | `rs.example.com`            |
| `storage.blazegraph`             | Blazegraph Persistent Volume Size                             | `2Gi`                       |
| `storage.images`                 | Volume Size for uploaded images                               | `2Gi`                       |
| `storage.platform`               | Volume Size for config files and ad-hoc templates             | `2Gi`                       |

Specify each parameter using the `--set key=value[,key=value]` argument to `helm install` or provide a `custom.yaml` file containing the values.

## Installation

```shell
helm repo add chart-museum https://charts.lincsproject.ca/
helm install --name research-space -f custom.yaml chart-museum/research-space
```

### Authentication

The default user is `admin` and the password is `admin`

## Uninstall

By default, a deliberate uninstall will result in the persistent volume claim being deleted.

```shell
helm delete research-space
```

To delete the deployment and its history:

```shell
helm delete --purge research-space
```
